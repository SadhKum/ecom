import { Selector,t } from "testcafe";

export default class HomePage {
     // all selectors 
    constructor () {
     this.categories = Selector("div.list-group").withText("CATEGORIES")
     this.laptopsCategory = Selector('#itemc').nth(1)
     this.monitorsCategory = Selector("a#itemc").withText("Monitors")
     this.sonyLaptop = Selector('#tbodyid a').withText('Sony vaio i7')
     this.asusMonitor = Selector("h4.card-title").withText("ASUS Full HD")
     this.addToCartButton = Selector('#tbodyid a').withText('Add to cart')
     this.homeOption = Selector('#navbarExample a').withText('Home')
     this.cartNavigate = Selector('#cartur')
     this.getCartList = Selector(".table-responsive")
     this.laptopItemInCart = Selector("#tbodyid > tr > td:nth-of-type(2)").withText("Sony vaio i7")
     this.monitorItemInCart = Selector("#tbodyid > tr:nth-of-type(2) > td:nth-of-type(2)").withText("ASUS Full HD")
      this.homePageButton  = Selector('#navbarExample a').withText('Home')
    
    }
    
      

}