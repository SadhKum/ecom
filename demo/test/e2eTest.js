import { ClientFunction } from "testcafe";
import HomePage from "./pages/homePage.js";
import { Selector,t } from "testcafe"; 

const URL = "https://www.demoblaze.com/"
const getURL = ClientFunction (()=> window.location.href);
const homePage = new HomePage();

fixture ("E2E Test for ecommerce project")
.page(URL)


test("Add the Sony Vaio i7 laptop and ASUS Full HD monitor into the shopping cart" , async t => {
 await t
  .maximizeWindow()
  .expect(getURL()).eql(URL)
  .click(homePage.laptopsCategory)
  .click(homePage.sonyLaptop)
  .setNativeDialogHandler(() => true)  // handle the alert 
  .click(homePage.addToCartButton)
  .click(homePage.homePageButton)
  .click(homePage.monitorsCategory)
  .click(homePage.asusMonitor)
  .setNativeDialogHandler(() => true)
  .click(homePage.addToCartButton)
  .click('#cartur')                   // go to cart page 
  .wait(20000)
  .expect(homePage.laptopItemInCart.innerText).contains("Sony vaio i7")
  .expect(homePage.monitorItemInCart.innerText).contains("ASUS Full HD")

})




